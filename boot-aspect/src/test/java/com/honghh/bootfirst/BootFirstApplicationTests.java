package com.honghh.bootfirst;

import com.honghh.bootfirst.mq.Sender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootFirstApplicationTests {
    @Autowired
    Sender sender;

    @Test
    public void contextLoads() {
        sender.send("msg--send", 10000);
    }

}
