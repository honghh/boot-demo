package com.honghh.bootfirst.cos;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.GetObjectRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;

import java.io.File;

/**
 * ClassName: QcloudTest
 * Description:
 *
 * @author honghh
 * @date 2019/02/28 14:10
 * Copyright (C) 杭州同基汽车科技有限公司
 */
public class QcloudTest {
    public static void main(String[] args) {
        // 1 初始化用户身份信息（secretId, secretKey）。
        COSCredentials cred = new BasicCOSCredentials("AKID7HDuHPnQ8b9RgiOkkW3s54rPFvABiiA1",
                "vc6KMuH5emfOvD7pyubS5V4PvC1hH7sh");
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        // clientConfig中包含了设置 region, https(默认 http), 超时, 代理等 set 方法, 使用可参见源码或者接口文档 FAQ 中说明。
        ClientConfig clientConfig = new ClientConfig(new Region("ap-guangzhou"));
        // 3 生成 cos 客户端。
        COSClient cosClient = new COSClient(cred, clientConfig);
        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
        String bucketName = "file-1253796769";

        File localFile = new File("D:\\images\\backgroud1.jpg");
        // 指定要上传到 COS 上对象键
        String putKey = "images/backgroud1.png";

        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, putKey, localFile);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        System.out.println(putObjectResult.toString());

        // 指定要下载到的本地路径
        File downFile = new File("D:\\images\\微信截图_20181208173917.png");

        // 指定文件在 COS 上的对象键
        String key = "images/微信截图_20181208173917.png";

        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);
        ObjectMetadata downObjectMeta = cosClient.getObject(getObjectRequest, downFile);
        System.out.println(downObjectMeta.toString());
    }
}
