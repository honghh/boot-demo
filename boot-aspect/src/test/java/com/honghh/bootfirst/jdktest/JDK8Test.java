package com.honghh.bootfirst.jdktest;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

/**
 * ClassName: JDK8Test
 * Description:
 *
 * @author honghh
 * @date 2019/02/28 10:12
 */
public class JDK8Test {
    public static void main(String args[]) {

        List<String> names1 = new ArrayList<>();
        names1.add("Google ");
        names1.add("Runoob ");
        names1.add("Taobao ");
        names1.add("Baidu ");
        names1.add("Sina ");
        names1.add("Sina ");
        names1.add("");
        System.out.println("=========》排序");
        System.out.println("=========》1.排序");
        names1.stream().sorted().forEach(System.out::println);
        System.out.println("=========》2.排序");
        Collections.sort(names1, (s1, s2) -> s1.compareTo(s2));
        System.out.println(names1);

        System.out.println("=========》遍历");

        System.out.println("=========》1.遍历");
        names1.forEach(System.out::println);

        System.out.println("=========》2.遍历");
        names1.forEach(n -> System.out.println(n));
        System.out.println("=========》去重");
        List unique = names1.stream().distinct().collect(Collectors.toList());
        System.out.println(unique);

        System.out.println("=========》过滤空元素");
        List<String> filtered = names1.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println(filtered);

        List<User> userList = new ArrayList<>();
        userList.add(new User(1L, "zhangsan"));
        userList.add(new User(2L, "lisi"));
        userList.add(new User(3L, "wangwu"));
        userList.add(new User(4L, "mazi"));
        userList.add(new User(5L, "zhangsan"));
        userList.add(new User(6L, "zhangsan"));
        userList.add(new User(7L, "zhangsan"));

        System.out.println("=========》对象 遍历");
        userList.forEach(item -> System.out.println("id " + item.getId() + ", name " + item.getName()));

        System.out.println("=========》对象 去重");
        List<User> uniqueUsers =  userList.stream().collect(
                collectingAndThen(
                        toCollection(() -> new TreeSet<>(comparing(User::getName))), ArrayList::new));
        System.out.println(uniqueUsers.toString());
    }


}
