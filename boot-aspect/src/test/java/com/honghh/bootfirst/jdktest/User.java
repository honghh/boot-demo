package com.honghh.bootfirst.jdktest;

import java.math.BigDecimal;

/**
 * ClassName: User
 * Description:
 *
 * @author honghh
 * @date 2019/02/28 10:38
 */
public class User {

    private Long id;

    private String name;

    private Integer age;

    private BigDecimal amount;

    public User() {
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(Long id, String name, Integer age, BigDecimal amount) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
