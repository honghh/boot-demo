package com.honghh.bootfirst.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.honghh.bootfirst.dao.InterfaceLogMapper;
import com.honghh.bootfirst.entity.InterfaceLog;
import com.honghh.bootfirst.service.InterfaceLogService;
import org.springframework.stereotype.Service;

/**
 * ClassName: InterfaceLogServiceImpl
 * Description:
 *
 * @author honghh
 * @date 2019/03/11 15:05
 */
@Service
public class InterfaceLogServiceImpl extends ServiceImpl<InterfaceLogMapper, InterfaceLog> implements InterfaceLogService {

    @Override
    public int saveLog(InterfaceLog interfaceLogDO) {
        return baseMapper.insert(interfaceLogDO);
    }
}
