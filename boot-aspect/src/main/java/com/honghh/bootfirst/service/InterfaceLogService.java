package com.honghh.bootfirst.service;

import com.honghh.bootfirst.entity.InterfaceLog;

/**
 * ClassName: InterfaceLogService
 * Description:
 *
 * @author honghh
 * @date 2019/03/11 15:05
 */
public interface InterfaceLogService {

    int saveLog(InterfaceLog interfaceLogDO);
}
