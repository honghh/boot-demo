package com.honghh.bootfirst.mq;

import com.honghh.bootfirst.constants.Constants;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * ClassName: ImmediateReceiver
 * Description:
 *
 * @author honghh
 * @date 2019/03/08 16:41
 */
@Component
@EnableRabbit
@Configuration
public class Receiver {

    @RabbitListener(queues = Constants.IMMEDIATE_QUEUE)
    @RabbitHandler
    public void get(String booking) {
        System.out.println(new Date() + " 收到延时消息了： " +  booking);
    }
}
