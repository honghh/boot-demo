package com.honghh.bootfirst.mq;

import com.honghh.bootfirst.constants.Constants;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName: ImmediateSender
 * Description:
 *
 * @author honghh
 * @date 2019/03/08 16:41
 */
@Component
public class Sender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * Description : 发送延迟消息
     * Group :
     *
     * @author honghh
     * @date  2019/3/8 0008 18:03
     * @param msg
     * @param delayTime
     */
    public void send(String msg, int delayTime) {
        System.out.println("delayTime：[ms]" + delayTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.rabbitTemplate.convertAndSend(
                Constants.DEAD_LETTER_EXCHANGE,
                Constants.DELAY_ROUTING_KEY,
                msg,
                message -> {
            message.getMessageProperties().setExpiration(delayTime + "");
            System.out.println(sdf.format(new Date()) + " 发送完成.");
            return message;
        });
    }
}
