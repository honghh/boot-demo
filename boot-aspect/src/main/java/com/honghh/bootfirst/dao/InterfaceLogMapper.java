package com.honghh.bootfirst.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.honghh.bootfirst.entity.InterfaceLog;

/**
 * ClassName: InterfaceLogMapper
 * Description:
 *
 * @author honghh
 * @date 2019/03/11 16:05
 */
public interface InterfaceLogMapper extends BaseMapper<InterfaceLog> {

}
