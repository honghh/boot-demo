package com.honghh.bootfirst.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;


/**
 * ClassName: InterfaceLog
 * Description:
 *
 * @author honghh
 * @date 2019/03/11 15:05
 */
@Data
@TableName("interface_log")
public class InterfaceLog implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	*
	*/
	@TableId(type = IdType.AUTO)
	private Long id;
	/**
	* 请求url
	*/
	private String url;
	/**
	* 请求类型
	*/
	private String requestType;
	/**
	* 请求方式
	*/
	private String requestMethod;
	/**
	* 请求报文
	*/
	private String requestBody;
	/**
	* 请求时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date requestDate;
	/**
	* 响应报文
	*/
	private String responseBody;
	/**
	* 响应状态值
	*/
	private String responseStatus;
	/**
	* 响应消息
	*/
	private String responseMessage;
	/**
	* 响应时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private java.util.Date responseDate;
	/**
	* 操作者IP地址
	*/
	private String remoteAddr;
	/**
	* 备注
	*/
	private String remark;
	/**
	* 创建人id
	*/
	@TableField(value = "creator_id", fill = FieldFill.INSERT)
	private Long creatorId;
	/**
	* 创建人名称
	*/
	@TableField(value = "creator", fill = FieldFill.INSERT)
	private String creator;
	/**
	* 创建时间
	*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@TableField(value = "create_date", fill = FieldFill.INSERT)
	private java.util.Date createDate;
	/**
	* 是否有效
	*/
	@TableLogic
	private Boolean valid;

}
