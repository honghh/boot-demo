package com.honghh.bootfirst.service.impl;

import com.honghh.bootfirst.service.CustomUserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * ClassName: CustomUserServiceImpl
 * Description:
 *
 * @author honghh
 * @date 2019/02/25 13:38
 */
@Service
public class CustomUserServiceImpl implements CustomUserService {
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Set<GrantedAuthority> authorities = new HashSet<>();
        if("admin".equalsIgnoreCase(username)) {
            // 添加权限
            authorities.add(new SimpleGrantedAuthority("test"));
            authorities.add(new SimpleGrantedAuthority("admin"));
        } else {
            authorities.add(new SimpleGrantedAuthority("test"));
        }
        User user = new User(username, "123456", authorities);
        return user;
    }
}
