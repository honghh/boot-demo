package com.honghh.bootfirst.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * ClassName: CustomUserService
 * Description:
 *
 * @author honghh
 * @date 2019/02/25 13:36
 */
public interface CustomUserService extends UserDetailsService {

}
