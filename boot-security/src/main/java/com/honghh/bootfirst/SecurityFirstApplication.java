package com.honghh.bootfirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityFirstApplication.class, args);
    }

}
