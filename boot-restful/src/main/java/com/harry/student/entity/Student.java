package com.harry.student.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * @author honghh
 * Date 2021-10-01 13:26:31
 * Copyright (C) www.tech-harry.cn
 */
@TableName("student")
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;

    private String studentId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 院系
     */
    private String department;
    /**
     * 专业
     */
    private String major;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }
}
