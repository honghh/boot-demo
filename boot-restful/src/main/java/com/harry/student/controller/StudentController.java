package com.harry.student.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.harry.common.api.CommonPage;
import com.harry.common.api.CommonResult;
import com.harry.student.entity.Student;
import com.harry.student.service.StudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * @author honghh
 * Date 2021-10-01 13:26:31
 * Copyright (C) www.tech-harry.cn
 */
@RestController
@RequestMapping("api/v1/student")
public class StudentController {
    @Resource
    private StudentService studentService;

    @GetMapping()
    public CommonResult<CommonPage<Student>> list(Student student,
                                                  @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
                                                  @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum) {
        IPage<Student> page = studentService.getPage(student, pageSize, pageNum);
        return CommonResult.success(CommonPage.restPage(page));
    }

    @PostMapping()
    public CommonResult<Integer> create(@RequestBody Student student) {
        int count = studentService.create(student);
        if (count > 0) {
            return CommonResult.success(count);
        }
        return CommonResult.failed();
    }

    @PutMapping(value = "/{studentId}")
    public CommonResult<Integer> update(@PathVariable String studentId, @RequestBody Student student) {
        int count = studentService.update(studentId, student);
        if (count > 0) {
            return CommonResult.success(count);
        }
        return CommonResult.failed();
    }

    @DeleteMapping(value = "/{studentId}")
    public CommonResult<Integer> deleteByIds(@PathVariable String studentId) {
        int count = studentService.deleteById(studentId);
        if (count > 0) {
            return CommonResult.success(count);
        }
        return CommonResult.failed();
    }
}
