package com.harry.student.service.impl;

import com.harry.student.dao.StudentDao;
import com.harry.student.entity.Student;
import com.harry.student.service.StudentService;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;


/**
 *
 *
 * @author honghh
 * Date 2021-10-01 13:26:31
 * Copyright (C) www.tech-harry.cn
 */
@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {


    @Override
    public IPage<Student> getPage(Student student, Integer pageSize, Integer pageNum) {
        LambdaQueryWrapper<Student> wrapper = new LambdaQueryWrapper<>(student);

        return page(new Page<>(pageNum, pageSize), wrapper);
    }


    @Override
    public Student selectById(Long id) {
        return this.baseMapper.selectById(id);
    }

    @Override
    public int create(Student student) {
        return this.baseMapper.insert(student);
    }


    @Override
    public int update(String studentId, Student student) {
        student.setStudentId(studentId);
        return this.baseMapper.updateById(student);
    }


    @Override
    public int deleteById(String studentId) {
        return this.baseMapper.deleteById(studentId);
    }



}