package com.harry.student.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.harry.student.entity.Student;

/**
 * 
 *
 * @author honghh
 * Date 2021-10-01 13:26:31
 * Copyright (C) www.tech-harry.cn
 */
public interface StudentService {

    /**
     * 分页查询信息
     *
     * @param student
     * @param pageSize
     * @param pageNum
     * @return
     */
    IPage<Student> getPage(Student student, Integer pageSize, Integer pageNum);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    Student selectById(Long id);

    /**
     * 创建信息
     *
     * @param student
     * @return
     */
    int create(Student student);

    /**
     * 更新
     *
     * @param studentId
     * @param student
     * @return
     */
    int update(String studentId, Student student);

    /**
     * 删除
     *
     * @param studentId
     * @return
     */
    int deleteById(String studentId);
}

