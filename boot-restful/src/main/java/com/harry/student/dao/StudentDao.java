package com.harry.student.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.harry.student.entity.Student;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author honghh
 * Date 2021-10-01 13:26:31
 * Copyright (C) www.tech-harry.cn
 */
@Mapper
public interface StudentDao extends BaseMapper<Student> {
	
}
