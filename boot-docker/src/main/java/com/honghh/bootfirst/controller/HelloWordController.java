package com.honghh.bootfirst.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: HelloWordController
 * Description:
 *
 * @author honghh
 * @date 2020/01/20 9:50
 */
@RestController
public class HelloWordController {
    @RequestMapping("/")
    public String index() {
        return "Hello Docker!";
    }
}
