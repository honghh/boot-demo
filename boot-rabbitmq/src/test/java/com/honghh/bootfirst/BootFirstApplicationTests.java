package com.honghh.bootfirst;

import com.honghh.bootfirst.mq.ImmediateSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BootFirstApplicationTests {
    @Autowired
    ImmediateSender immediateSender;
    @Test
    public void contextLoads() {
        immediateSender.send("book", 1000);
    }

}
