package com.honghh.bootfirst.constants;

/**
 * ClassName: Constants
 * Description:
 *
 * @author honghh
 * @date 2019/03/08 16:37
 */
public class Constants {

    public static final String IMMEDIATE_QUEUE ="immediate_queue" ;
    public static final String IMMEDIATE_EXCHANGE = "immediate_exchange" ;
    public static final String IMMEDIATE_ROUTING_KEY = "immediate_routing_key";
    public static final String DELAY_QUEUE = "delay_queue";
    public static final String DEAD_LETTER_EXCHANGE ="dead_letter_exchange" ;
    public static final String DELAY_ROUTING_KEY = "delay_routing_key";
}
