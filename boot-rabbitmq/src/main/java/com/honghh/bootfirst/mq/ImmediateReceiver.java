package com.honghh.bootfirst.mq;

import com.honghh.bootfirst.constants.Constants;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * ClassName: ImmediateReceiver
 * Description:
 *
 * @author honghh
 * @date 2019/03/08 16:41
 * Copyright (C) 杭州同基汽车科技有限公司
 */
@Component
@EnableRabbit
@Configuration
public class ImmediateReceiver {

    @RabbitListener(queues = Constants.IMMEDIATE_QUEUE)
    @RabbitHandler
    public void get(String booking) {
        System.out.println("收到延时消息了" + booking);
    }
}
