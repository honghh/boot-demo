package com.honghh.bootfirst.mq;

import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

/**
 * ClassName: Receiver
 * Description: 消息接收者
 *
 * @author honghh
 * @date 2019/03/06 16:07
 */
@Component
public class Receiver {

    private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message) {
        System.out.println("Received <" + message + ">");
        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

}
